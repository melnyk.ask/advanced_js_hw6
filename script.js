let btn = document.querySelector(".btn");

btn.onclick = async () => {

    let out = document.querySelector(".out");
    out.textContent = "Please, wait..."

    let ip = await getIP();
    
    let info = await getInfo(ip);

    showInfo(info);
}

async function getInfo(ip){
    let request = await fetch(`http://ip-api.com/json/${ip}?fields=status,message,continent,continentCode,country,countryCode,region,regionName,city,district,query`);
    let result = await request.json();
    // console.log(result);
    return result;
}

async function getIP(){
    let request = await fetch("https://api.ipify.org/?format=json");
    let result = await request.json();
    // console.log(result);
    return result.ip;
}

function showInfo(json_info){
    let out = document.querySelector(".out");
    out.innerHTML = `
        <ul>
            <li>Континент: ${json_info.continent}, ${json_info.continentCode}</li>
            <li>Країна: ${json_info.country}, ${json_info.countryCode}</li>
            <li>Регіон: ${json_info.region}, ${json_info.regionName}</li>
            <li>Місто: ${json_info.city}</li>
            <li>Район: ${json_info.district}</li>
        </ul>
    `;
}